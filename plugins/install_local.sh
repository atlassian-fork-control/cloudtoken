#!/bin/bash

export VERSION=0.1.0

PLUGINS=$(ls -d *-plugin)

for PLUGIN in ${PLUGINS}
do
    cd ${PLUGIN}
    pip3 install -e .
    cd ..
done
